/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Negocio.ListaNumeros;
import java.util.Scanner;

/**
 *
 * @author Ryzen
 */
public class TestOrdenarSeleccion {
      public static void main(String[] args) {
        
        ListaNumeros lista= new ListaNumeros(leerEntero("Digite la cantidad de elementos: "));
        for(int i=0; i<lista.lenght(); i++){
            lista.adicionar(i, leerFloat("Digite dato ["+ i +"]:"));
        }
        lista.ordenar_Seleccion();
        System.out.println("Su lista es ordenada es: "+ lista.toString());
       
        
        
    }
    
    private static float leerFloat(String msg){
        System.out.print(msg);
        Scanner in = new Scanner(System.in);
        try{
             return in.nextFloat();
        } catch (java.util.InputMismatchException ex){
                System.err.println("Error no es un Float");
                return leerFloat(msg);
        }
    }
    
    private static int leerEntero(String msg){
        System.out.print(msg);
        Scanner in = new Scanner(System.in);
        try{
             return in.nextInt();
        } catch (java.util.InputMismatchException ex){
                System.err.println("Error no es un Entero");
                return leerEntero(msg);
        }
    }    
}
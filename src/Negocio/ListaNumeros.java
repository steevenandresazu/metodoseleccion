/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Nuestra primer estructtua : vector ---> lista numeros
 * @author steeven sayago 1152018
 */
public class ListaNumeros implements Comparable{
    
    Scanner lector = new Scanner (System.in);
    private float [] numeros;

    public ListaNumeros(){
    }
    
    
    public ListaNumeros (int cant) {
    
        if(cant<=0){
            throw new RuntimeException("No se puede crear el vector");
            
        }
        this.numeros= new float[cant];
        
    }
    /**
     *  Adiciona un numero en la posicion i
     * @param i indice donde se va a ingresar el dato
     * @param numeroNuevo el dato a ingresar
     */
    public void adicionar(int i,float numeroNuevo){
        this.validar(i);
        this.numeros[i]=numeroNuevo;
    }
    
    
    private float getElemento(int i){
        this.validar(i);
        return this.numeros[i];
        
    }
    
    
    private void validar (int i){
        if(i<0 || i >= this.numeros.length){
            throw new RuntimeException("indice fuera de rango"+i);  
        }
        
        
    }
    
    
    
    /**
     * 
     * REDUNDANTE
     */
    private void actualizar(int i,float numeroNuevo){
        this.adicionar(i, numeroNuevo);
        
    }
    public float[] getNumeros() {
        return numeros;
    }

    public void setNumeros(float[] numeros) {
        this.numeros = numeros;
    }

    @Override
    public String toString() {
        String msg="";
        /*
        cuando solo es recorrido se usa forEach
        */
        for(float dato:this.numeros){
            msg+=dato+"\t";
        }
        return msg;
        
    }
    
    
    public int lenght(){
        
        return this.numeros.length;
    }
    /**
     * elimina la posicion i y redimenciona el vector
     * @param i la posicion a eliminar
     */
    
    
    public void eliminar(int i){
        
    }
    /**
     * ordeena el vector por el metodo de la burbuja
     */
    public void ordenar_Burbuja(){
        
        
        
        for(int i=0; i<(numeros.length-1);i++){
            for(int j=0; j<(numeros.length-1); j++){
                if(numeros[j]>numeros[j+1]){
                    float aux = numeros[j];
                    numeros[j] = numeros[j+1];
                    numeros[j+1]= aux;
                            
                }
                
            }
            
        }
        
    }
    
    public void decreciente(){
        System.out.println("Arreglo en forma decreciente: ");
        for(int i=(numeros.length-1);i>=0; i--){
            System.out.println(numeros[i]+"");
        }
    }
    
    
    /**
     * ordena el vector por el metodo de seleccion
     */
    
    public void ordenar_Seleccion(){
        
        int i,j;  //Variables para los For
       
        for(i = 0; i < this.numeros.length-1; i++){ 
            int posMenor=i; //Variable que guarda la posicion menor que va encontrando
            
            
            for(j = i+1; j<this.numeros.length; j++){ //desde i+1 para una posicion mayor a i, 
                if(this.numeros[j]< numeros[posMenor])//si arreglo posicion j si es menor a i.
                {
                    posMenor=j; // guarda el dato menor j en posMenor
                }
            }
         float iaux =  this.numeros[i]; //guarda el arreglo en la posicion i, la primer posicion recorrida
         this.numeros[i] = this.numeros[posMenor]; //guarda en posicion i la posicion menor
         numeros[posMenor]= iaux; //realiza el cambio de posiciones
        }
          
    }
    
    /**
     *  retorna una lista de numeros con la union de conjuntos
     * de lista original con lista dos
     * 
     * se debe validar que la lista nueva contenga unicamente
     * la cantidad de celdas requeridas
     * 
     * ejemplo: ¨this={3 4 5 6} y dos={3 4}
     * listaNueva = {3 4 5 6}
     * @param dos una lista de numeros 
     * @return  una nueva lista con la union de conjuntos
     */
    public ListaNumeros getUnion(ListaNumeros dos){
        
        return null;
    }
    
    /**
     *  retorna una lista de numeros con la interseccion de conjuntos
     * de lista original con lista dos
     * 
     * se debe validar que la lista nueva contenga unicamente
     * la cantidad de celdas requeridas
     * 
     * ejemplo: ¨this={3 4 5 6} y dos={3 4}
     * listaNueva = {3 4 }
     * @param dos una lista de numeros 
     * @return  una nueva lista con la interseccion de conjuntos
     */
    public ListaNumeros getInterseccion(ListaNumeros dos){
        
        return null;
    }
    
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 31 * hash + Arrays.hashCode(this.numeros);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ListaNumeros other = (ListaNumeros) obj;
        
        if(this.numeros.length != other.numeros.length){
            return false;
        }
        
        for(int i = 0; i<this.numeros.length;i++){
            if(this.numeros[i]!= other.numeros[i]){
                return false;
            }
        }
         return true;

    }
    /**
     * una lista es mayor a otra si y solo si
     * 1. tienen el mismo tamaño
     * 2. todos los elementos de la lista 1 son mayores a los de las lista 2
     * en orden suponga que la lista 1 y 2 se encuenta ordenada
     * @param o
     * @return 
     */
    @Override
    public int compareTo(Object o) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
    
    
    
}
